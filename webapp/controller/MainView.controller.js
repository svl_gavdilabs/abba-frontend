sap.ui.define([
  "sap/ui/core/mvc/Controller",
  "sap/ui/model/json/JSONModel",
  "sap/m/MessageToast"
], function(Controller, JSONModel, MessageToast) {
  "use strict";

  return Controller.extend("com.gavdilabs.abba.controller.MainView", {
    onInit: function () {
			var oCitiesModel = new JSONModel(sap.ui.require.toUrl("sap/f/sample/Card/model/cities.json")),
				oProductsModel =  new JSONModel(sap.ui.require.toUrl("sap/f/sample/Card/model/products.json"));

			this.getView().setModel(oCitiesModel, "cities");
			this.getView().setModel(oProductsModel, "products");
		}
  });
});
